#!/usr/bin/env python3
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler

def predictData(train_file_path, to_predict_file_path):
	train_csv = pd.read_csv(train_file_path)
	to_predict_csv = pd.read_csv(to_predict_file_path)
	bulk_csv = pd.concat([train_csv, to_predict_csv], ignore_index=True)

	x_data = bulk_csv.iloc[:,:5]
	x_data.fillna(0, inplace=True)
	y_train = train_csv.iloc[:,5]

	# normalizacion de data
	estandarizacion = StandardScaler().fit_transform(x_data)
	x_data_norm = pd.DataFrame(data=estandarizacion, columns=x_data.columns)

	x_train = x_data_norm.iloc[0:len(train_csv),:5]
	# corregir que se pega el index anterior y deberia de empezar de 0 otra vez
	x_test = x_data_norm.iloc[len(train_csv):,:5]

	# creamos el modelo de rn y se entrena
	mlp = MLPClassifier(hidden_layer_sizes=(2,2), max_iter=30, learning_rate_init=0.2, activation='logistic')
	mlp.fit(x_train, y_train)
	result = mlp.predict(x_test)
	return result

def saveResult(to_predict_file_path, result):
	to_predict_csv = pd.read_csv(to_predict_file_path)
	table = to_predict_csv.iloc[:,:5]
	table.fillna(0, inplace=True)
	rain = pd.DataFrame(data=result)
	table["RAIN"] = rain
	table.to_csv('result.csv', index=False,)
	pass

def separteByMonths(file_path):
	data = pd.read_csv(file_path)
	months = data['MONTH'].values

	# resultsByMonth = []
	# for x in range(12):
	# 	resultByMonth = []
	# 	for row in data.rows:
	# 		if(row[1] == x+1):

def getRainFromFile(file_path):
	data = pd.read_csv(file_path)
	return data['RAIN'].values