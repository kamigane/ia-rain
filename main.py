from tkinter import Tk,Button,Label,Entry,filedialog,messagebox
from PIL import Image as ImageP,ImageTk as ImageTkP 
from tkinter import *
from rain_predictor import predictData,saveResult,getRainFromFile
import matplotlib.pyplot as plt

root=Tk()

global fileDataSet,filePredicSet,fileRealSet
global dataSet, predictSet, realSet
global labelResult

dataSet = StringVar()
predictSet = StringVar()
realSet = StringVar()

def askFile(type):
    if type == "dataset":
        fileDataSet = filedialog.askopenfile(title = "Seleccionar Dataset",filetypes = (("archivos csv","*.csv"),("all files","*.*")))
        if(fileDataSet is not None):
            dataSet.set(fileDataSet.name)
    elif type == "predictset":
        filePredicSet = filedialog.askopenfile(title = "Seleccionar Dataset a Predecir",filetypes = (("archivos csv","*.csv"),("all files","*.*")))
        if(filePredicSet is not None):
            predictSet.set(filePredicSet.name)
    elif type == "realset":
        fileRealSet = filedialog.askopenfile(title = "Seleccionar Dataset Real",filetypes = (("archivos csv","*.csv"),("all files","*.*")))
        if(fileRealSet is not None):
            realSet.set(fileRealSet.name)


def showImageLogo():
    load = ImageP.open("assets/logo.png")
    load.resize((100,100), ImageP.ANTIALIAS)
    render = ImageTkP.PhotoImage(load)
    img = Label(root, image=render,width=100,height=100)
    img.image = render
    img.grid(row=0,column=1,pady=(20,10),padx=(100,0))

def showBrowseDataSet():
    Label(root, text="Dataset: ").grid(row=1,column=0,padx=3)
    Entry(root, textvariable=dataSet,state='disabled',width=48).grid(row=1,column=1)
    Button(root,text="Buscar dataset",command=lambda: askFile("dataset")).grid(row=1,column=2,padx=(10,0))

def showPredictDataSet():
    Label(root, text="Predecir varios: ").grid(row=2,column=0,padx=3)
    Entry(root, textvariable=predictSet,state='disabled',width=48).grid(row=2,column=1)
    Button(root,text="Buscar dataset",command=lambda: askFile("predictset")).grid(row=2,column=2,padx=(10,0))
    Button(root,text="Predecir Lluvia",command=handlePredict).grid(row=2,column=3,padx=(10,0))

def showLabelResultFile():
    labelResult.grid(row=3,column=0,columnspan=4,padx=(10,0))

def showCompareDataSet():
    Label(root, text="Dataset a comparar: ").grid(row=4,column=0,padx=3,pady=(50,0))
    Entry(root, textvariable=realSet,state='disabled',width=48).grid(row=4,column=1,pady=(50,0))
    Button(root,text="Buscar dataset",command=lambda: askFile("realset")).grid(row=4,column=2,padx=(10,0),pady=(50,0))
    Button(root,text="Comparar resultados",command=handleCompare).grid(row=4,column=3,columnspan=2,padx=(10,0),pady=(50,0))


def handlePredict():
    if(dataSet.get() == "" or predictSet.get() == ""):
        messagebox.showwarning("Alerta","Debe seleccionar los dos dataset necesarios para continuar con la predicción.")
        return
    result = predictData(dataSet.get(),predictSet.get())
    saveResult(predictSet.get(),result)
    showLabelResultFile()
    showCompareDataSet()

def handleCompare():
    if(realSet.get() == ""):
        messagebox.showinfo("Alerta","Seleccionar el dataset de los valores reales para continuar.")
    
    rainResultFile = getRainFromFile('result.csv')
    rainRealFile = getRainFromFile(realSet.get())

    resultDays = range(1,rainResultFile.size + 1)
    realDays = range(1,rainRealFile.size + 1)
    
    resultRain = [ int(x) for x in rainResultFile.tolist()]
    realRain = [ int(x) for x in rainRealFile.tolist()]

    plt.plot(resultDays, resultRain,'g-',label="Predicción")
    plt.plot(realDays, realRain,'k-',label="Real")
    plt.ylabel('Resultado')
    plt.xlabel('Dias')
    plt.legend(bbox_to_anchor=(1.1, 1.05))
    plt.show()

showImageLogo()
showBrowseDataSet()
showPredictDataSet()

labelResult = Label(root,text="Los resultados fueron guardados en el archivo: result.csv")

root.geometry("850x400")
root.title("APLICACIÓN PARA PREDICCIÓN DEL CLIMA SEATTLE")
root.mainloop()

root.mainloop()
